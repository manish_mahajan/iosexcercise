//
//  UIImageView+Extension.swift
//  iOSExcercise
//
//  Created by Manish Mahajan on 18/12/19.
//  Copyright © 2019 Manish Mahajan. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension UIImageView {
    
    //Download for this image view using provided path
    //Set default image if image not found
    func downloadImage(_ path: String) {
        if let url = URL(string: path) {
           self.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "default"), options:.refreshCached)
        } else {
            self.image = #imageLiteral(resourceName: "default")
        }
    }
}
