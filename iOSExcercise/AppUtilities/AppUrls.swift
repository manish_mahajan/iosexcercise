//
//  AppUrls.swift
//  iOSExcercise
//
//  Created by Manish Mahajan on 18/12/19.
//  Copyright © 2019 Manish Mahajan. All rights reserved.
//

import Foundation

class AppUrls {
    
    static let BASE_PATH = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/"
    static let FACTS = "facts.json"
}
