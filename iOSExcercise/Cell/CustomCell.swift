//
//  CustomCell.swift
//  iOSExcercise
//
//  Created by Manish Mahajan on 18/12/19.
//  Copyright © 2019 Manish Mahajan. All rights reserved.
//

import Foundation
import UIKit


class CustomTableCell: UITableViewCell {
    
    //Setup each cell
    var row: Rows? {
        didSet {
            guard let contactItem = row else {return}
            
            title.text = contactItem.title ?? ""
            profileImageView.downloadImage(contactItem.imageHref ?? "")
            titleDescription.text = contactItem.description ?? ""
        }
    }
    
    //Setup profile image
    let profileImageView:UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.translatesAutoresizingMaskIntoConstraints = false
        img.layer.cornerRadius = 35
        img.clipsToBounds = true
        return img
    }()
    
    //Setup title label
    let title:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = .black
        label.numberOfLines = 0
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    //Setup description label
    let titleDescription:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .black
        label.clipsToBounds = true
        label.numberOfLines = 0
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    //Setup cell layout using Layout constraint
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(profileImageView)
        self.contentView.addSubview(title)
        self.contentView.addSubview(titleDescription)
        
        profileImageView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant:10).isActive = true
        profileImageView.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor, constant:10).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant:70).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant:70).isActive = true
        contentView.bottomAnchor.constraint(greaterThanOrEqualTo: self.profileImageView.bottomAnchor, constant: 10).isActive = true
        
        title.topAnchor.constraint(equalTo:self.profileImageView.topAnchor).isActive = true
        title.leadingAnchor.constraint(equalTo:self.profileImageView.trailingAnchor, constant: 10).isActive = true
        title.trailingAnchor.constraint(equalTo:self.contentView.trailingAnchor, constant: -10).isActive = true
        
        titleDescription.topAnchor.constraint(equalTo: self.title.bottomAnchor, constant: 4).isActive = true
        titleDescription.leadingAnchor.constraint(equalTo: self.title.leadingAnchor).isActive = true
        titleDescription.trailingAnchor.constraint(equalTo: self.title.trailingAnchor).isActive = true
        contentView.bottomAnchor.constraint(greaterThanOrEqualTo: self.titleDescription.bottomAnchor, constant: 10).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
}
