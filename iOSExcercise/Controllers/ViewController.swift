//
//  ViewController.swift
//  iOSExcercise
//
//  Created by Manish Mahajan on 18/12/19.
//  Copyright © 2019 Manish Mahajan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private let cellId = "cell"
    var model: ResponseModel!
    
    //Setup refresh control on table view drag
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.black
        
        return refreshControl
    }()
    
    //Setup tableview 
    lazy var tableView: UITableView = {
        
        let tv = UITableView(frame: .zero, style: .plain)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.delegate = self
        tv.dataSource = self
        tv.register(CustomTableCell.self, forCellReuseIdentifier: self.cellId)
        tv.addSubview(self.refreshControl)
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(tableView)
        setupAutoLayout()
        getData()
    }
    
    //Get API call
    func getData() {
        
        let url = AppUrls.BASE_PATH + AppUrls.FACTS
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request, completionHandler: { data, response, error -> Void in
            do {
                if (error != nil) {
                    
                } else {
                    //Because of null data need to convert into string
                    let s1 = String(data: data!, encoding: String.Encoding.ascii)!
                    let data1 = Data(s1.utf8)
                    
                    self.model = try JSONDecoder().decode(ResponseModel.self, from: data1)
                    DispatchQueue.main.async {
                        self.navigationController?.navigationBar.topItem!.title = self.model.title ?? ""
                        self.tableView.reloadData()
                        self.refreshControl.endRefreshing()
                    }
                }
            } catch {
                print("Error", error)
                self.refreshControl.endRefreshing()
            }
        }).resume()
    }
    
    //Setup Auto Layout for table view
    func setupAutoLayout() {
        
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        getData()
    }
}

//Implmentation of UITableView Delegate and Datasource methods
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.model?.rows?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! CustomTableCell
        if let rows = self.model.rows {
            cell.row = rows[indexPath.row]
        }
        return cell
    }
}
